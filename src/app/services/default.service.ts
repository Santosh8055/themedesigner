import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DefaultService {
  private textStyles = {
    'font-family': 'calibri',
    'font-size': '16px',
    color: 'black',
    'font-weight': 'normal',
    'font-style': 'normal',
    'text-decoration': 'none',
    'vertical-align': 'initial',
    'padding-top': '0',
    'padding-right': '0',
    'padding-bottom': '0',
    'padding-left': '0'
  };

  public borderStyles = {
    'border-bottom-width': '0',
    'border-left-width': '0',
    'border-right-width': '0',
    'border-top-width': '0',
    'border-style': 'none',
    'border-color': 'initial'
  };

  constructor() {
    for (const key in this.styles) {
      if (this.styles.hasOwnProperty(key)) {
        this.styles[key] = { ...this.styles[key], ...this.textStyles };
      }
    }
    const borderComponents = ['table', 'thead', 'tbody', 'aggregation', 'groupAggregation', 'form'];
    for (const component of borderComponents) {
      this.styles[component] = { ...this.styles[component], ...this.borderStyles };
    }
    this.styles.additional = { subAggregation: 'row', aggregation: 'row' };
  }
  public styles: StyleComponents = {
    text: {},
    title: {
      'background-color': 'transparent',
      'text-align': 'left'
    },
    table: {
      'border-type': 'all'
    },
    thead: {
      'background-color': 'transparent',
      'text-align': 'left'
    },
    tbody: {},
    aggregation: {
      'background-color': 'transparent',
      'text-align': 'left'
    },
    groupAggregation: {
      'background-color': 'transparent',
      'text-align': 'left'
    },
    form: {},
    formLabel: {
      'label-align': 'none'
    },
    list: {
      type: 'ordered',
      'list-format': '1'
    }
  };
  public fontFamily = [
    {
      name: 'Serif',
      value: 'Serif'
    },
    {
      name: 'Georgia',
      value: 'Georgia'
    },
    {
      name: 'Palatino Linotype',
      value: 'Palatino Linotype'
    },
    {
      name: 'Book Antiqua',
      value: 'Book Antiqua'
    },
    {
      name: 'Palatino',
      value: 'Palatino'
    },
    {
      name: 'Times New Roman [Times,serif]',
      value: 'Times New Roman,Times,serif'
    },
    {
      name: 'Arial',
      value: 'Arial'
    },
    {
      name: 'Arial Black',
      value: 'Arial Black'
    },
    {
      name: 'Gadget',
      value: 'Gadget'
    },
    {
      name: 'Comic Sans MS',
      value: 'Comic Sans MS'
    },
    {
      name: 'Cursive',
      value: 'Cursive'
    },
    {
      name: 'Impact',
      value: 'Impact'
    },
    {
      name: 'Charcoal',
      value: 'Charcoal'
    },
    {
      name: 'Lucida Sans Unicode',
      value: 'Lucida Sans Unicode'
    },
    {
      name: 'Lucida Grande',
      value: 'Lucida Grande'
    },
    {
      name: 'Tahoma',
      value: 'Tahoma'
    },
    {
      name: 'Trebuchet MS',
      value: 'Trebuchet MS'
    },
    {
      name: 'Verdana',
      value: 'Verdana'
    },
    {
      name: 'Geneva',
      value: 'Geneva'
    },
    {
      name: 'Sans Serif',
      value: 'Sans-serif'
    },
    {
      name: 'Courier New',
      value: 'Courier New'
    },
    {
      name: 'Courier',
      value: 'Courier'
    },
    {
      name: 'Lucida Console',
      value: 'Lucida Console'
    },
    {
      name: 'Monaco',
      value: 'Monaco'
    },
    {
      name: 'Monospace',
      value: 'Monospace'
    },
    {
      name: 'Calibri',
      value: 'Calibri'
    }
  ];

  public verticalAlignment = [
    {
      name: 'Top',
      value: 'top'
    },
    {
      name: 'Middle',
      value: 'middle'
    },
    {
      name: 'Bottom',
      value: 'bottom'
    }
  ];

  public textAlign = [
    {
      name: 'Left',
      value: 'left'
    },
    {
      name: 'Right',
      value: 'right'
    },
    {
      name: 'Center',
      value: 'center'
    }
  ];
  public borderStyle = [{ name: 'Solid', value: 'solid' }, { name: 'None', value: 'none' }];

  public borderType = [
    { name: 'All columns', value: 'all' },
    { name: 'Table border', value: 'table' },
    { name: 'Border bottom', value: 'border-bottom' },
    { name: 'Header bottom', value: 'header-bottom' },
    { name: 'Custom', value: 'none' }
  ];

  public applyStylesFor = [{ name: 'For whole row', value: 'row' }, { name: 'Aggregation cell', value: 'cell' }];

  public labelAlign = [
    {
      name: 'None',
      value: 'none'
    },
    {
      name: 'Left',
      value: 'left'
    },
    {
      name: 'Right',
      value: 'right'
    },
    {
      name: 'Top',
      value: 'top'
    }
  ];

  public listType = [
    { name: 'Ordered', value: 'ordered' },
    { name: 'Unordered', value: 'unordered' },
    { name: 'None', value: 'none' }
  ];

  public listFormat = [
    { name: '1', value: '1' },
    { name: 'a', value: 'a' },
    { name: 'A', value: 'A' },
    { name: 'i', value: 'i' },
    { name: 'I', value: 'I' }
  ];

  public fontWeight = [{ name: 'Normal', value: '' }, { name: 'Bold', value: 'bold' }];
  public fontStyle = [{ name: 'Normal', value: '' }, { name: 'Italic', value: 'italic' }];
  public textDecoration = [{ name: 'None', value: 'none' }, { name: 'Underline', value: 'underline' }];
}
