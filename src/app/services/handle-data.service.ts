import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';
import { DefaultService } from './default.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class HandleDataService {
  private baseURL = 'http://sergio.vistex.local:8000/sap/bc/gtms2/api/object/StatementDesigner/Theme';
  public processedTheme: Theme;
  public selectedComponent: any;
  public themeName: string;

  public alternateRow = { colors: false, tableEvenRow: '', tableOddRow: '' };

  constructor(
    private http: HttpClient,
    private snackBar: MatSnackBar,
    public defaultService: DefaultService,
    private router: Router
  ) {}

  public getThemes() {
    return this.http.get(this.baseURL, {
      params: {
        'sap-client': '040',
        'sap-user': 'atalla',
        'sap-password': 'welcome'
      }
    });
  }

  public loadTheme(callback?: () => void): void {
    this.http
      .get(this.baseURL, {
        params: {
          id: this.themeName,
          'sap-client': '040',
          'sap-user': 'atalla',
          'sap-password': 'welcome'
        }
      })
      .pipe(catchError(this.handleError))
      .subscribe((data: BackEndTheme) => {
        if (!data.Data || !data.Data.NAME) {
          this.snackBar.open(`Theme ${this.themeName} not found`, 'ok', {
            duration: 3000
          });
          this.router.navigate(['./']);
          return;
        }
        this.processedTheme = { name: '', description: '' };
        this.processedTheme.name = data.Data.NAME;
        this.processedTheme.description = data.Data.DESCRIPTION;
        // tslint:disable-next-line: prefer-const
        let processedStyles = {};
        data.Data.STYLES.map((props: STYLE) => {
          const styles = JSON.parse(props.VALUE);

          if (props.NAME === 'tableEvenRow' || props.NAME === 'tableOddRow') {
            this.alternateRow.colors = true;
            if (styles && styles['background-color']) {
              this.alternateRow[props.NAME] = styles['background-color'];
            }
          } else {
            processedStyles[props.NAME] = styles;
          }
        });
        this.processedTheme.styles = this.mergeDefaultStyles(processedStyles as StyleComponents);
        this.selectedComponent = this.initializeSelectedComponent();
        if (callback) {
          callback();
        }
      });
  }

  public initializeSelectedComponent() {
    let initialStyleComponent = null;
    if (this.processedTheme && this.processedTheme.styles) {
      initialStyleComponent = 'text';
    }
    return initialStyleComponent;
  }

  public sendTheme() {
    if (this.processedTheme) {
      const parsedTheme = {
        NAME: this.processedTheme.name,
        DESCRIPTION: this.processedTheme.description,
        STYLES: []
      };

      for (const key in this.processedTheme.styles) {
        if (this.processedTheme.styles.hasOwnProperty(key)) {
          const styleValue = { ...this.processedTheme.styles[key] };
          parsedTheme.STYLES.push({ NAME: key, VALUE: JSON.stringify(styleValue) });
        }
      }
      if (this.alternateRow.colors) {
        const color = {};
        color['background-color'] = this.alternateRow.tableEvenRow;
        parsedTheme.STYLES.push({ NAME: 'tableEvenRow', VALUE: JSON.stringify(color) });
        color['background-color'] = this.alternateRow.tableOddRow;
        parsedTheme.STYLES.push({ NAME: 'tableOddRow', VALUE: JSON.stringify(color) });
      }
      this.http
        .post(this.baseURL, parsedTheme, {
          params: {
            'sap-client': '040',
            id: this.processedTheme.name,
            'sap-user': 'atalla',
            'sap-password': 'welcome'
          }
        })
        .pipe(catchError(this.handleError))
        .subscribe(() => {
          this.snackBar.open('Theme saved!', 'ok', {
            duration: 1000
          });
        });
    }
  }

  public setTextStylesGlobally() {
    for (const key in this.processedTheme.styles) {
      if (this.processedTheme.styles.hasOwnProperty(key) && key !== 'text') {
        this.processedTheme.styles[key] = { ...this.processedTheme.styles[key], ...this.processedTheme.styles.text };
      }
    }
  }

  public setSelectedComponent(style: { name: string; value: object }) {
    if (!this.selectedComponent || !this.selectedComponent.name || this.selectedComponent.name !== style.name) {
      this.selectedComponent = style;
    }
  }

  // public changeTableBorderColor(color: string) {
  //   const sides = ['top', 'bottom', 'left', 'right'];

  //   for (const side of sides) {
  //     this.processedTheme.styles.table[`border-${side}-color`] = color;
  //     this.processedTheme.styles.thead[`border-${side}-color`] = color;
  //     this.processedTheme.styles.tbody[`border-${side}-color`] = color;
  //   }
  // }

  public changeTableBorderStyle(styleType: string) {
    const sides = ['top', 'bottom', 'left', 'right'];

    const tableBorderStyle = styleType === 'table' || styleType === 'all' ? '0.1' : '0';
    const theadBorderStyle = styleType === 'all' ? '0.1' : '0';
    const tbodyBorderStyle = styleType === 'all' ? '0.1' : '0';

    this.processedTheme.styles.table['border-style'] = 'solid';
    this.processedTheme.styles.tbody['border-style'] = 'solid';
    this.processedTheme.styles.thead['border-style'] = 'solid';

    for (const side of sides) {
      this.processedTheme.styles.table[`border-${side}-width`] = tableBorderStyle;
      this.processedTheme.styles.tbody[`border-${side}-width`] = tbodyBorderStyle;
      this.processedTheme.styles.thead[`border-${side}-width`] = theadBorderStyle;
    }

    if (styleType === 'border-bottom') {
      this.processedTheme.styles.tbody[`border-bottom-width`] = '0.1';
      this.processedTheme.styles.thead[`border-bottom-width`] = '0.1';
    } else if (styleType === 'header-bottom') {
      this.processedTheme.styles.thead[`border-bottom-width`] = '0.1';
    }
  }
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
    }
    return throwError('Something bad happened; please try again later.');
  }

  private mergeDefaultStyles(backEndStyles: StyleComponents): StyleComponents {
    const mergedComponents = {};
    const defaultStyles = this.defaultService.styles;
    for (const key in defaultStyles) {
      if (defaultStyles.hasOwnProperty(key)) {
        // if (backEndStyles[key]) {
        //   mergedComponents[key] = { ...defaultStyles[key], ...backEndStyles[key] };
        // } else {
        //   mergedComponents[key] = { ...defaultStyles[key] };
        // }
        if (backEndStyles[key]) {
          for (const style in defaultStyles[key]) {
            if (defaultStyles[key].hasOwnProperty(style)) {
              if (backEndStyles[key][style]) {
                defaultStyles[key][style] = backEndStyles[key][style];
              }
            }
          }
        }
        mergedComponents[key] = { ...defaultStyles[key] };
      }
    }
    return mergedComponents as StyleComponents;
  }
}
