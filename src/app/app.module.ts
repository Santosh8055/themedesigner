import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ThemeListComponent } from './theme-list/theme-list.component';
import { ThemeDetailComponent } from './theme-detail/theme-detail.component';
import {
  MatToolbarModule,
  MatIconModule,
  MatCardModule,
  MatListModule,
  MatInputModule,
  MatFormFieldModule,
  MatSidenavModule,
  MatProgressSpinnerModule,
  MatButtonModule,
  MatSnackBarModule,
  MatAutocompleteModule,
  MatSelectModule,
  MatSlideToggleModule,
  MatButtonToggleModule,
  MatTooltipModule
} from '@angular/material';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { DescriptioncasePipe } from './descriptioncase.pipe';
import { StyleFieldComponent } from './style-field/style-field.component';
import { PreviewBlockComponent } from './preview-block/preview-block.component';
import { ColorFieldComponent } from './color-field/color-field.component';
import { NumberFieldComponent } from './number-field/number-field.component';
import { SelectFieldComponent } from './select-field/select-field.component';
import { ToggleButtonComponent } from './toggle-button/toggle-button.component';

import { LayoutModule } from '@angular/cdk/layout';

@NgModule({
  declarations: [
    AppComponent,
    ThemeListComponent,
    ThemeDetailComponent,
    DescriptioncasePipe,
    StyleFieldComponent,
    PreviewBlockComponent,
    ColorFieldComponent,
    NumberFieldComponent,
    SelectFieldComponent,
    ToggleButtonComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatListModule,
    MatInputModule,
    MatFormFieldModule,
    MatSidenavModule,
    MatProgressSpinnerModule,
    MatButtonModule,
    MatSnackBarModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatButtonToggleModule,
    MatTooltipModule,
    LayoutModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
