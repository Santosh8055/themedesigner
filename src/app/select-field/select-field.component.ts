import { Component, OnInit, Input } from '@angular/core';
import { HandleDataService } from '../services/handle-data.service';
import { DefaultService } from './../services/default.service';

@Component({
  selector: 'app-select-field',
  templateUrl: './select-field.component.html',
  styleUrls: ['./select-field.component.scss']
})
export class SelectFieldComponent implements OnInit {
  @Input() selectModel: string;
  @Input() selectList: string;
  constructor(public handleData: HandleDataService, public defaultService: DefaultService) {}
  ngOnInit() {}

  public onValueChange(): void {
    if (this.handleData.selectedComponent === 'table' && this.selectModel === 'border-type') {
      const styleType = this.handleData.processedTheme.styles[this.handleData.selectedComponent][this.selectModel];
      this.handleData.changeTableBorderStyle(styleType);
    }
  }
}
