import { Component, OnInit, Input } from '@angular/core';
import { HandleDataService } from '../services/handle-data.service';

@Component({
  selector: 'app-number-field',
  templateUrl: './number-field.component.html',
  styleUrls: ['./number-field.component.scss']
})
export class NumberFieldComponent implements OnInit {
  @Input() numberModel: string;
  @Input() suffix: string;
  constructor(public handleData: HandleDataService) {}

  ngOnInit() {}
}
