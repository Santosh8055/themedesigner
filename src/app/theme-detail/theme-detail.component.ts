import { Component, OnInit } from '@angular/core';
import { HandleDataService } from './../services/handle-data.service';
import { ActivatedRoute } from '@angular/router';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';

@Component({
  selector: 'app-theme-detail',
  templateUrl: './theme-detail.component.html',
  styleUrls: ['./theme-detail.component.scss']
})
export class ThemeDetailComponent implements OnInit {
  public theme: object;
  public loadPreviewBlock = false;
  public styleComponents = [];
  public processedStyles: StyleComponents;

  public isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map(result => result.matches),
    shareReplay()
  );

  constructor(
    public handleData: HandleDataService,
    private route: ActivatedRoute,
    private breakpointObserver: BreakpointObserver
  ) {}

  ngOnInit() {
    this.handleData.loadTheme(() => {
      this.styleComponents = this.objectKeys(this.handleData.processedTheme.styles);
      this.styleComponents.pop();
      this.processedStyles = this.handleData.processedTheme.styles;
    });

    // perfomance improvement
    this.loadPreviewBlock = true;
  }

  public objectKeys(obj: any) {
    return Object.keys(obj);
  }

  public textGlobalApply() {
    this.handleData.setTextStylesGlobally();
  }

  toggleTheme(event: MouseEvent) {
    const icon = event.currentTarget as HTMLElement;
    icon.classList.toggle('night-mode');
    document.querySelector('body').classList.toggle('dark-theme');
  }
}
