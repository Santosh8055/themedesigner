import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'descriptioncase'
})
export class DescriptioncasePipe implements PipeTransform {
  transform(value: string, from: string = 'camelcase'): string {
    let formatedValue = '';
    if (from === 'camelcase') {
      formatedValue = value.charAt(0).toUpperCase() + value.slice(1).replace(/([A-Z])/g, ' $1');
    } else if (from === 'kebabcase') {
      formatedValue = value
        .split('-')
        .join(' ')
        .replace(/\w\S*/g, txt => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase());
    }
    return formatedValue;
  }
}
