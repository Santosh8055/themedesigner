import { Component, OnInit } from '@angular/core';
import { HandleDataService } from '../services/handle-data.service';

@Component({
  selector: 'app-preview-block',
  templateUrl: './preview-block.component.html',
  styleUrls: ['./preview-block.component.scss']
})
export class PreviewBlockComponent implements OnInit {
  public processedStyles: StyleComponents;
  public tableCells: Array<string>;
  public tableData: Array<Array<any>>;

  public countries: Array<{ name: string; start: number }>;
  constructor(public handleData: HandleDataService) {}

  ngOnInit() {
    this.processedStyles = this.handleData.processedTheme.styles;
    this.tableCells = ['Country', 'Firstname', 'Lastname', 'Company', 'Salary'];
    this.tableData = [
      ['Pace', 'Yen', 'Mauris Elit Dictum Incorporated', 4360],
      ['Reynolds', 'Jena', 'Magnis Dis Corporation', 2441],
      ['Hammond', 'Driscoll', 'Facilisi Corporation', 4368],
      ['Farmer', 'Ivory', 'Fringilla Est Mauris Consulting', 3414],
      ['Leonard', 'Ross', 'Aliquam Auctor Institute', 3770],
      ['Riggs', 'Cyrus', 'Non Dapibus Rutrum Institute', 4568],
      ['Mcconnell', 'Forrest', 'Ac Turpis Limited', 4059],
      ['Harding', 'Jason', 'Aliquet Molestie Tellus Incorporated', 1740],
      ['Garner', 'Hanae', 'Sem Industries', 1561],
      ['Bentley', 'Hilda', 'Lorem Auctor Quis Institute', 3280],
      ['Spence', 'Benedict', 'Elit Erat Vitae Associates', 2978],
      ['Bishop', 'Griffith', 'Nonummy Foundation', 3858],
      ['Mcdowell', 'Orson', 'Donec Corporation', 2810],
      ['Hicks', 'Lacy', 'Vel Corporation', 2165],
      ['Howell', 'Raphael', 'Porttitor Company', 2381],
      ['Cantu', 'Ciara', 'Dolor Sit Amet Inc.', 3071],
      ['Barry', 'Holly', 'Duis Cursus Foundation', 2259],
      ['Matthews', 'Levi', 'Magna A Neque Company', 1394],
      ['Combs', 'Gage', 'Vel LLP', 1458],
      ['Hickman', 'Dane', 'Duis Dignissim Foundation', 3562],
      ['Mayer', 'Boris', 'Est Vitae Sodales Ltd', 2742],
      ['Head', 'Ralph', 'Vitae Ltd', 3648],
      ['Anthony', 'Selma', 'Penatibus Et Magnis Company', 2352],
      ['Raymond', 'Chaim', 'Vestibulum Ante Ipsum Inc.', 1506],
      ['Reyes', 'Fallon', 'Ut Pellentesque Incorporated', 3786],
      ['Suarez', 'Carlos', 'Lacus Quisque Imperdiet Corp.', 3800],
      ['Dennis', 'Igor', 'Sagittis Foundation', 3085],
      ['Hines', 'Jasmine', 'Vel Convallis In Industries', 4079],
      ['Ewing', 'Dorothy', 'Eu Dolor Egestas Company', 3509],
      ['Kemp', 'Wayne', 'Nascetur Ridiculus Limited', 3196],
      ['Poole', 'Tatiana', 'Nunc Nulla Vulputate Corporation', 4390],
      ['Molina', 'Haley', 'Neque In Ornare Corp.', 1210],
      ['Cochran', 'Alexa', 'Erat Sed Inc.', 2663],
      ['Barry', 'Gareth', 'Nunc Quis Ltd', 3460],
      ['Hampton', 'Rinah', 'Blandit Limited', 4811],
      ['Murphy', 'Deacon', 'Non Vestibulum Nec Company', 4308],
      ['Nieves', 'Pandora', 'Dui Cum Sociis LLP', 3094],
      ['Schneider', 'Jenna', 'Orci Adipiscing Non Associates', 3563],
      ['Ortiz', 'Jordan', 'Eros Non Corp.', 1263],
      ['Peters', 'Tamara', 'Ultrices Posuere Consulting', 3040],
      ['Fitzpatrick', 'Solomon', 'Tristique Neque Limited', 2293],
      ['Frederick', 'Colt', 'Egestas Blandit Nam PC', 3847],
      ['Hatfield', 'Roary', 'Nam Foundation', 2188],
      ['Pratt', 'Bianca', 'Donec Non Incorporated', 4117],
      ['Kramer', 'Penelope', 'Quis Massa Mauris LLP', 2638],
      ['Sellers', 'Kelsey', 'Enim Suspendisse Aliquet Institute', 3141],
      ['Taylor', 'Caesar', 'Mi Aliquam Company', 1581],
      ['West', 'Colin', 'Mauris Morbi Inc.', 2951],
      ['Vinson', 'Candace', 'Turpis In Condimentum Incorporated', 3219],
      ['Huffman', 'Deacon', 'Nullam Ut Corp.', 4780],
      ['Gilmore', 'Rae', 'Nascetur LLC', 4910],
      ['Beard', 'Nehru', 'Metus Industries', 3916],
      ['Chang', 'Randall', 'Non LLC', 1581],
      ['Hewitt', 'Mariko', 'Sit Amet Consectetuer Industries', 4020],
      ['Wong', 'Lillian', 'Tellus Associates', 3524],
      ['Boyd', 'Addison', 'Aliquam Ultrices Associates', 2334],
      ['Chan', 'Tatyana', 'Conubia Nostra Per Industries', 4520],
      ['Olsen', 'Piper', 'Duis Institute', 4984],
      ['Collins', 'Diana', 'Eget Metus Inc.', 2066],
      ['Santiago', 'Brody', 'Est Associates', 2621],
      ['Schroeder', 'Hu', 'Dignissim PC', 3554],
      ['Peterson', 'Sigourney', 'Dis Parturient Ltd', 2752],
      ['Buckner', 'Camilla', 'At LLC', 4974],
      ['Head', 'Bertha', 'Mauris Suspendisse Aliquet Consulting', 2056],
      ['Schroeder', 'Eaton', 'Eu Enim Etiam Institute', 4291],
      ['Bird', 'Jelani', 'Elit Pharetra Ut Incorporated', 4301],
      ['Cantu', 'Kane', 'Nulla Associates', 4679],
      ['Harvey', 'Raphael', 'Eleifend Corporation', 1235],
      ['Anthony', 'Wanda', 'Vitae Company', 4867],
      ['Forbes', 'Calvin', 'Et Limited', 1321],
      ['Griffin', 'Kiona', 'Sed Malesuada Ltd', 1041],
      ['Cabrera', 'Eve', 'Mi Felis Adipiscing LLP', 4645],
      ['Page', 'Isaiah', 'Vestibulum Consulting', 4957],
      ['Hunter', 'Chanda', 'Congue LLC', 2888],
      ['Blake', 'Hannah', 'Purus Accumsan Associates', 1851],
      ['Head', 'Aaron', 'Dolor Nonummy Ac Ltd', 1082],
      ['Mckenzie', 'Hamilton', 'Nascetur Ridiculus Corporation', 4106],
      ['Hurley', 'Madonna', 'Faucibus Corporation', 4966],
      ['Whitney', 'Shad', 'Fringilla Institute', 1513],
      ['Barker', 'Abraham', 'Nec LLC', 2825],
      ['Townsend', 'Kelly', 'Interdum Enim Non PC', 1184],
      ['Mendoza', 'Deacon', 'Nunc Sed Orci LLC', 3046],
      ['Mckee', 'Jolene', 'Donec Egestas Ltd', 1061],
      ['Donovan', 'Avye', 'Sit Amet Institute', 4144],
      ['Pena', 'Bo', 'Phasellus Libero Mauris Incorporated', 4425],
      ['Mckenzie', 'Felix', 'Id Limited', 3995],
      ['Stevenson', 'Hector', 'Ipsum Donec Institute', 3705],
      ['Whitaker', 'Anika', 'At Augue Incorporated', 4671],
      ['Parks', 'Willa', 'Auctor Vitae Aliquet LLC', 4006],
      ['Mann', 'Gabriel', 'Dictum Proin Eget Industries', 4471],
      ['Vazquez', 'Gil', 'Nonummy Ac Feugiat Incorporated', 4328],
      ['Stanley', 'Rae', 'Ultricies Dignissim Lacus Corp.', 1357],
      ['Finley', 'Dara', 'Ultricies Corp.', 3591],
      ['Lane', 'Geoffrey', 'Erat Etiam LLP', 3958],
      ['Willis', 'Luke', 'Mauris Sagittis Placerat Limited', 2668],
      ['Holden', 'Kevin', 'Felis Adipiscing Limited', 3963],
      ['Leblanc', 'Karen', 'Ultricies Corporation', 1062],
      ['Holloway', 'Wallace', 'Elit Dictum Eu Foundation', 1590],
      ['Riggs', 'Vladimir', 'Non Incorporated', 4984],
      ['Harrington', 'Shana', 'Commodo Associates', 2316]
    ];
    this.countries = [{ name: 'Italy', start: 0 }, { name: 'Germany', start: 5 }, { name: 'Austria', start: 9 }];
  }

  public getCompleteStyles(prop: string): object {
    const completeStyles = { ...this.processedStyles[prop] };
    // font
    completeStyles['font-size'] += 'px';

    // padding
    completeStyles['padding-top'] += 'mm';
    completeStyles['padding-bottom'] += 'mm';
    completeStyles['padding-left'] += 'mm';
    completeStyles['padding-right'] += 'mm';

    // border
    const borderComponents = ['table', 'thead', 'tbody', 'aggregation', 'groupAggregation', 'form'];
    if (borderComponents.includes(prop)) {
      const sides = ['top', 'bottom', 'left', 'right'];
      for (const side of sides) {
        completeStyles[`border-${side}-width`] += 'mm';
      }
    }

    if (prop === 'formLabel') {
      delete completeStyles['label-align'];
    }
    return completeStyles;
  }

  getSum(field: number, start: number, end: number): number {
    let total = 0;
    for (let i = start; i <= end; i++) {
      total += this.tableData[i][field];
    }
    return total;
  }
}
