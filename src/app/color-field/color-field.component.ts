import { Component, OnInit, Input } from '@angular/core';
import { HandleDataService } from '../services/handle-data.service';

@Component({
  selector: 'app-color-field',
  templateUrl: './color-field.component.html',
  styleUrls: ['./color-field.component.scss']
})
export class ColorFieldComponent implements OnInit {
  @Input() colorModel: string;
  constructor(public handleData: HandleDataService) {}

  ngOnInit() {}
}
