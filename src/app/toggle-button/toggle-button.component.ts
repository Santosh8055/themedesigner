import { Component, OnInit, Input } from '@angular/core';
import { HandleDataService } from './../services/handle-data.service';

@Component({
  selector: 'app-toggle-button',
  templateUrl: './toggle-button.component.html',
  styleUrls: ['./toggle-button.component.scss']
})
export class ToggleButtonComponent implements OnInit {
  @Input() bProps: any;
  constructor(public handleData: HandleDataService) {}

  ngOnInit() {}

  public toggleSelectedValue(prop: string, value: string): void {
    if (this.handleData.processedTheme.styles[this.handleData.selectedComponent][prop] !== value) {
      this.handleData.processedTheme.styles[this.handleData.selectedComponent][prop] = value;
    } else {
      if (this.bProps.type !== 'set') {
        this.handleData.processedTheme.styles[this.handleData.selectedComponent][prop] = '';
      }
    }
  }
}
