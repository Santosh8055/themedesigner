import { Component, OnInit } from '@angular/core';
import { HandleDataService } from '../services/handle-data.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-theme-list',
  templateUrl: './theme-list.component.html',
  styleUrls: ['./theme-list.component.scss']
})
export class ThemeListComponent implements OnInit {
  public themes: Array<object>;

  constructor(public hD: HandleDataService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.hD.themeName = '';
    this.route.queryParams.subscribe(params => {
      if (params && params.name) {
        this.hD.themeName = params.name;
      } else {
        this.hD.getThemes().subscribe((data: { Data: Array<object> }) => {
          this.hD.themeName = '';
          this.themes = data.Data;
        });
      }
    });
  }
}
