import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StyleFieldComponent } from './style-field.component';

describe('StyleFieldComponent', () => {
  let component: StyleFieldComponent;
  let fixture: ComponentFixture<StyleFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StyleFieldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StyleFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
