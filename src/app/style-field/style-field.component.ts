import { Component, OnInit } from '@angular/core';
import { HandleDataService } from '../services/handle-data.service';
import { DefaultService } from './../services/default.service';

@Component({
  selector: 'app-style-field',
  templateUrl: './style-field.component.html',
  styleUrls: ['./style-field.component.scss']
})
export class StyleFieldComponent implements OnInit {
  public processedStyles: StyleComponents;
  public fontStyleButtons = [
    { prop: 'font-weight', tValue: 'bold', icon: 'format_bold', type: 'toggle' },
    { prop: 'font-style', tValue: 'italic', icon: 'format_italic', type: 'toggle' },
    { prop: 'text-decoration', tValue: 'underline', icon: 'format_underline', type: 'toggle' }
  ];

  public textAlignButtons = [
    { prop: 'text-align', tValue: 'left', icon: 'format_align_left', type: 'set' },
    { prop: 'text-align', tValue: 'center', icon: 'format_align_center', type: 'set' },
    { prop: 'text-align', tValue: 'right', icon: 'format_align_right', type: 'set' }
  ];

  constructor(public handleData: HandleDataService, public defaultService: DefaultService) {}

  ngOnInit() {
    this.processedStyles = this.handleData.processedTheme.styles;
  }
  public objectKeys(obj: any): Array<string> {
    return Object.keys(obj);
  }

  public toggleSelectedValue(prop: string, value: string): void {
    if (this.processedStyles[this.handleData.selectedComponent][prop] === value) {
      this.processedStyles[this.handleData.selectedComponent][prop] = '';
    } else {
      this.processedStyles[this.handleData.selectedComponent][prop] = value;
    }
  }

  public setSelectedValue(prop: string, value: string): void {
    if (this.processedStyles[this.handleData.selectedComponent][prop] !== value) {
      this.processedStyles[this.handleData.selectedComponent][prop] = value;
    }
  }
}
