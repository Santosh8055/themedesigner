interface StyleComponents {
  text?: FontText;
  title?: Title;
  table?: Table;
  thead?: Thead;
  tbody?: Tbody;
  aggregation?: Thead;
  groupAggregation?: Thead;
  form?: Form;
  formLabel?: FormLabel;
  list?: List;
  additional?: Additional;
}

interface Additional {
  subAggregation?: string;
  aggregation?: string;
}

interface List {
  type?: string;
  'list-format'?: string;
  'font-family'?: string;
  'font-size'?: number;
  color?: string;
  'font-weight'?: string;
  'font-style'?: string;
  'text-decoration'?: string;
  'vertical-align'?: string;
  'padding-top'?: string;
  'padding-right'?: string;
  'padding-bottom'?: string;
  'padding-left'?: string;
}

interface FormLabel {
  'label-align'?: string;
  'font-family'?: string;
  'font-size'?: number;
  color?: string;
  'font-weight'?: string;
  'font-style'?: string;
  'text-decoration'?: string;
  'vertical-align'?: string;
  'padding-top'?: string;
  'padding-right'?: number;
  'padding-bottom'?: string;
  'padding-left'?: string;
}

interface Form {
  'font-family'?: string;
  'font-size'?: number;
  color?: string;
  'font-weight'?: string;
  'font-style'?: string;
  'text-decoration'?: string;
  'vertical-align'?: string;
  'padding-top'?: number;
  'padding-right'?: number;
  'padding-bottom'?: number;
  'padding-left'?: number;
  'border-bottom-width'?: number;
  'border-left-width'?: number;
  'border-right-width'?: number;
  'border-top-width'?: number;
  'border-style'?: string;
  'border-color'?: string;
}

interface Tbody {
  'font-family'?: string;
  'font-size'?: number;
  color?: string;
  'font-weight'?: string;
  'font-style'?: string;
  'text-decoration'?: string;
  'vertical-align'?: string;
  'padding-top'?: number;
  'padding-right'?: number;
  'padding-bottom'?: number;
  'padding-left'?: number;
  'border-bottom-width'?: string;
  'border-left-width'?: string;
  'border-right-width'?: string;
  'border-top-width'?: string;
  'border-style'?: string;
  'border-color'?: string;
}

interface Thead {
  'background-color'?: string;
  'text-align'?: string;
  'font-family'?: string;
  'font-size'?: number;
  color?: string;
  'font-weight'?: string;
  'font-style'?: string;
  'text-decoration'?: string;
  'vertical-align'?: string;
  'padding-top'?: number;
  'padding-right'?: number;
  'padding-bottom'?: number;
  'padding-left'?: number;
  'border-bottom-width'?: string;
  'border-left-width'?: string;
  'border-right-width'?: string;
  'border-top-width'?: string;
  'border-style'?: string;
  'border-color'?: string;
}

interface Table {
  'border-type'?: string;
  'font-family'?: string;
  'font-size'?: number;
  color?: string;
  'font-weight'?: string;
  'font-style'?: string;
  'text-decoration'?: string;
  'vertical-align'?: string;
  'padding-top'?: string;
  'padding-right'?: string;
  'padding-bottom'?: string;
  'padding-left'?: string;
  'border-bottom-width'?: string;
  'border-left-width'?: string;
  'border-right-width'?: string;
  'border-top-width'?: string;
  'border-style'?: string;
  'border-color'?: string;
}

interface Title {
  'background-color'?: string;
  'text-align'?: string;
  'font-family'?: string;
  'font-size'?: number;
  color?: string;
  'font-weight'?: string;
  'font-style'?: string;
  'text-decoration'?: string;
  'vertical-align'?: string;
  'padding-top'?: string;
  'padding-right'?: string;
  'padding-bottom'?: string;
  'padding-left'?: string;
}

interface FontText {
  'font-family'?: string;
  'font-size'?: number;
  color?: string;
  'font-weight'?: string;
  'font-style'?: string;
  'text-decoration'?: string;
  'vertical-align'?: string;
  'padding-top'?: string;
  'padding-right'?: string;
  'padding-bottom'?: string;
  'padding-left'?: string;
}
interface Theme {
  name: string;
  description: string;
  styles?: StyleComponents;
}

interface BackEndTheme {
  Data: {
    NAME: string;
    DESCRIPTION: string;
    STYLES: STYLE[];
  };
}
interface STYLE {
  NAME: string;
  VALUE: string;
}
